import groovy.json.JsonOutput

// KEY: write json to file

def arr = ['a', 'b']

def data = [
    name : "Justin Bieber",
    year : 2020,
    timestamp : "2020-03-01T00:00:00",
    array : arr,
    pi    : 3.1415
]

def json_str = JsonOutput.toJson(data)
println(json_str)

// KEY: write string to file, write str to file
def file = new File('/tmp/f.x')
file.write(json_str)


