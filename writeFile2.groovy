import static classfile.Print.*;
import static classfile.Aron.*;
import classfile.StopWatch;
import classfile.Node;
import java.util.function.BiFunction;
import java.util.function.Function;

def sw = new StopWatch();

//class MyClass{
//    public MyClass(){
//    }
//}
//def c = new MyClass();
//
//for(int i=0; i<10; i++){
//    p(name);
//}
//
//def arr = ["dog", "cat", "fox"]
//writeFileAppend("/tmp/f1.x", arr)
//def str = executeCommand("ls /tmp");
//p("str=" + str);
//List<String> ls = listDir("/tmp");
//p(ls);
//def map = new HashMap<String, Integer>();
//def list = readFile "/tmp/f1.x"
//for(String s : list){
//    def ns = replaceStr(s, "dog", "cat")
//    p(ns)
//}
//
//def ld = listFileDir("/Users/cat/myfile/bitbucket/java")
//def mylist = []
//for(String s : ld){
//    if(isMatched('\\.java$', s)){
//        mylist.add(s)
//    }
//}
//fl()
//p(mylist)
//
// -------------------------------------------------------------------------------- 
// add code here




// -------------------------------------------------------------------------------- 
sw.printTime();

// Scanner scan = new Scanner(System.in);
// String s = scan.nextLine()
print('hi')


public static <T> void toFile(String fileName, List<T> list) {
    final String newline = newLine(); 
    try {
	FileWriter fstream = new FileWriter(fileName);
	BufferedWriter out = new BufferedWriter(fstream);
	for(T s : list){
            out.write(s.toString());
	}
	out.close();
    } catch(Exception e) {
	e.printStackTrace();
    }
}

public static <T> void writeFile2(String fName, List<T> ls){
    List<String> lsstr = list()
    for(T t : ls){
	if(t instanceof Collection){
	    var ss = map(r -> map(x -> x.toString(), r), ls);
	    lsstr = map(r -> drop(1, concat(" ", r) + "\n"), ss);
	}  
	else{
	    lsstr = list(drop(1, concat(" ", map(x -> x.toString(), ls))));
	}
    }
    
    toFile(fName, lsstr);
}


/*
public static <A, B> List<List<B>> map2(Function<A, B> f, List<List<A>> ls){
    return map(r -> map(x -> f.apply(x), r), ls);
}
*/



/*
List<Integer> ls = list(1, 2, 4, 4, 5)
writeFile("/tmp/x.x", ls)
 */


/*
public static <T> Node<T> node(T t){
    return new Node(t);
}
 */


List<List<Integer>> ss = list(list(1, 2), list(3, 4))
    writeFile2("/tmp/x1.x", ss)
    
List<Integer> mylist = list(4, 9, 3)
writeFile2("/tmp/x2.x", mylist)
print('ok1')


var lss = [[1, 2], [3, 4]]
var flss = [[0.3, 0.4], [4.3, 3.1415]]

List<List<String>> strlss = map2(x -> x.toString(), lss)
fl()
pp(strlss)
fl()
List<List<String>> strflss = map2(x -> x.toString(), flss)
pp(strflss)

var nodes = [[node(1), node(2)], [node(3), node(4)]]
var strNodes = map2(x -> x.toString(), nodes)
fl()
pp(strNodes)
fl()

/*
public static void writeFileNoNewLine(String fileName, List<String> list) {
    try {
	FileWriter fstream = new FileWriter(fileName);
	BufferedWriter out = new BufferedWriter(fstream);
	for(String s : list){
            out.write(s);
	}
	out.close();
    } catch(Exception e) {
    }
}

public static <T> void writeFile2d(String fname, List<List<T>> ls){
    final String newline = newLine(); 
    List<List<String>> lss = map2(x -> x.toString(), ls);
    List<String> strls = map(r -> drop(1, concat(" ", r)) + newline, lss);
    writeFileNoNewLine(fname, strls);
}
*/

def arr = [[2, 4], [3.1, 40.9]]
List<List<String>> ss4 = map2(x -> x.toString(), arr)
writeFile2d("/tmp/x3.x", ss4)

List<List<String>> s5 = list(list("a", "b"), list("c", "d"))
writeFile2d("/tmp/x4.x", arr)

def arr1 = [[1, 4], [3.1, 40.9], [4, 9, 3], [4, 1, 2, 4]]
writeFile2d("/tmp/x5.x", arr1)
