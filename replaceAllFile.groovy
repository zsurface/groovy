import static classfile.Print.*;
import static classfile.Aron.*;
import classfile.StopWatch;


StopWatch sw = new StopWatch();

// replace all files with regex
def flist = of()
listFile(".", flist); 

def html = '\\.html$'
def java = '\\.java$'
def groo = '\\.groovy$'



def spanOpen = "<span>"
def spanClose = "</span>"
def word = "String"
def regex = "\\b" + word + "\\b" 
def rep = spanOpen + word + spanClose


for(String fname : flist){
    if(isMatched(groo, fname)){
        fl()
        p(fname)
        def lines = readFile(fname)    
        for(String line : lines){
            def s = replaceStr(line, regex, rep);
            p("s=" + s);
        }
    }
}


def ss = replaceStr("dog [cat] aaa <cat> pig {cat} catrat", "\\bcat\\b", "KK");
p("bigs=" + ss)

sw.printTime();

Scanner scan = new Scanner(System.in);
String s = scan.nextLine()

