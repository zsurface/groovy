import static classfile.Print.*;
import static classfile.Aron.*;
import classfile.StopWatch;
import classfile.Tuple;

import groovy.sql.Sql;
import java.sql.*;
import java.io.File;

import com.mysql.jdbc.*;

// user: user1
// passwd: password
// database: testdb
// no SSL
// create table
// def url = 'jdbc:hsqldb:mem:yourDB'
//

var sp = getEnv("sp")
var ls = filter( x -> isMatched("Main.hs\$", x), lsFile(sp))
var lst = map( x -> tuple(x, fileModTimeSec(x)), ls)
var hmap = listToMap(lst)

Boolean done = false
while(!done){
    var ls1 = filter( x -> isMatched("Main.hs\$", x), lsFile(sp))
    var lst1 = map( x -> tuple(x, fileModTimeSec(x)), ls1)
    for(Tuple<String, Integer> t : lst1){
	Integer v = hmap.get(t.x)
	// pp("x=" + t.x)
	// pp("y=" + t.y)
	if(v != null && t.y > v){
	    pp(t)
	    
	    println("Main.hs is modified")
	    var splist = splitPath(t.x)
	    var rootpath = concatPath(take(len(splist) - 2, splist))
	    var runp = concatValidPath(list(rootpath, "run.sh"))
	    println("runp=" + runp)
	    var output = run(runp)
	    println(output)
	    println("rootpath=" + rootpath)
	    done = true
	}
    }
    // sleepSec(1)
}

// pp(ls)
// pp(lst)
// p(hmap)

