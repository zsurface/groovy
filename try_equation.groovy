import static classfile.Print.*
import classfile.Tuple;
import static classfile.Aron.*;
import classfile.Point2;
import java.util.*;

print('h')

Double mySum(List<Double> ls){
    Double s = 0.0
    for(Double d : ls)
	s += d;

    return s;
}


var numCurve = 10
var numPt = 5;


Point2<Double> p2(Double x, Double y, Double z){
    return new Point2(x, y, z);
}

/**
 Double fun(Double x, List<Double> ls){
    return ls.get(0)*x*x*x*x*x + ls.get(1)*x*x*x + ls.get(2)*x*x + ls.get(3)*x + ls.get(4)
 }
*/

var fixedPt = [p2(0.1, 0.8, 0), p2(0.2, 0.4, 0), p2(0.3, 0.1, 0), p2(0.4, 0.4, 0), p2(0.5, 0.7, 0)]

void writeFileFixedPt(pts){
    var fixedPtStr = map(x -> x.toStr() + "\n", pts)
    pp(fixedPtStr)
    writeFileList("/tmp/x.x", fixedPtStr)
}

Double quadraticEq(Double x, List<Double> ls){
    var a = ls.get(0)
    var b = ls.get(1)
    var c = ls.get(2)
    var d = ls.get(3)
    var e = ls.get(4)
    return a*x**5 + b*x**4 + c*x**2 + d*x + e
}

// var pt = [tuple(0.2, 0.6), tuple(0.3, 0.4), tuple(0.4, 0.8), tuple(0.5, 0.2), tuple(0.7, 0.8)]

// var point2 = [p2(0.1, 0.8, 0), p2(0.4, 0.4, 0), p2(0.3, 0.1, 0), p2(0.4, 0.7, 0), p2(0.5, 0.5, 0)]

writeFileFixedPt(fixedPt)

fl()
var abcListRandom = partList(numPt, randomList(numPt*numCurve))
pp(abcListRandom)



// given three fixed points such as p0(x, y), p1(x, y) and p2(x, y)
// generate some random triples (a_0, b_0, c_0) (a_1, b_1, c_1) .. (a_k, b_k, c_k)
// each triple (a_k, b_k, c_k) determines one curve
// compute the y'-coordiates of each fixed point on each curve
// compute the difference between p0.y and y'
// sum all the differences among three fixed points
var diffy = map(r -> map(p -> abs(quadraticEq(p.x, r) - p.y), fixedPt), abcListRandom)
fl("diffy")
pp(diffy)
fl()

var sumDiff = map(x -> mySum(x), diffy)
fl("sumDiff")
pp(sumDiff)
var z = zip( sumDiff, abcListRandom )

// sort the z according to x component
var sortz = sort(z, (a, b) -> a.x.compareTo(b.x))

// get the smallest value of x component with the triple (a_k, b_k, c_k)
var best = map(x -> x.toString(), head(sortz).y)

var bestPt = drop(1, concat(" ", best))

// writeFileAppend("/tmp/x.x", list("\n"))
writeFileList("/tmp/abc.x", list(bestPt))
pp(bestPt)
fl()
pp(z)
fl()
pp(sortz)



