import static classfile.Print.*;
import static classfile.Aron.*;
import classfile.StopWatch;

import groovy.sql.Sql;
import java.sql.*;
import java.io.File;

import com.mysql.jdbc.*;

// user: user1
// passwd: password
// database: testdb
// no SSL
// create table
// def url = 'jdbc:hsqldb:mem:yourDB'
//
if(false){
    def url = 'jdbc:mysql://localhost:3306/testdb?autoReconnect=true&useSSL=false'
    def user = 'user1'
    def password = 'password'
    // def driver = 'org.hsqldb.jdbcDriver'
    def driver = 'com.mysql.jdbc.Driver'
    def sql = Sql.newInstance(url, user, password, driver)
}

String pathToFileName(String path, int k){
    String s = fileName(path) + "_" + k + ".txt"
    return s
}
/**
     abaa
   
     a b
     a x
     a b a
     a x |
     a b a a
     a b x |
     a b x  => a b a

     a a b b
     x |
     x   |
     a   x |
     a   x   E
     a   b


     a a a b b
           |
     a a b b
     c   |
     
 */

var fPath = concatValidPath(list(getEnv("b"), "publicfile", "pathToDatabase.txt"))
var pathdir = concatValidPath(list(getEnv("b"), "publicfile", "pathdir"))
pp(fPath)
List<String> lsPath = readFile(fPath)


// sql.execute('DROP TABLE IF EXISTS tablepath')

// sql.execute('create table tablepath(id INT NOT NULL AUTO_INCREMENT, path VARCHAR(1000) NOT NULL, PRIMARY KEY(id))')

//We can also insert by prepared statements by
StopWatch sw = new StopWatch();


int k = 0
for(String path : lsPath){
    List<String> fList = list()
    String dirPath = concatValidPath(list(pathdir, fileName(path)))
    listAllFile(path, fList)
    var p = dirPath + "_" + k + ".txt"
    writeFileList(p, map( x -> x + "\n", fList))
    k++
}


/**
for(String s : fList){
    sql.execute('INSERT INTO tablepath VALUES(null,?)',[s])
}
*/

/**
List<String> lsetc = ls()
listAllFile("/etc", lsetc)
writeFileList("etc.txt", map(x -> x + "\n", lsetc))
 */

/**
for(String s : lsetc){
    sql.execute('INSERT INTO tablepath VALUES(null,?)',[s])
}
*/

/**
List<String> lsMyfile = ls();
listAllFile("/Users/cat/myfile", lsMyfile)
writeFileList("myfile.txt", map(x -> x + "\n", lsMyfile))
 */

/**
for(String s : lsMyfile){
    sql.execute('INSERT INTO tablepath VALUES(null,?)',[s])
}
*/

// use 'sql' instance ...

// sql.close()

sleepSec(3)
writeFileStr("/tmp/log.txt", longToStr(sw.diff()))
var sls = run("cat /tmp/log.txt")
pp(sls)
print("diff" + sw.diff())
sw.printSecond()
print("hikkkk")

