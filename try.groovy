import static classfile.Print.*;
import static classfile.Aron.*;
import classfile.StopWatch;

import groovy.sql.Sql
import java.sql.*
import java.io.File

import com.mysql.jdbc.*

// user: user1
// passwd: password
// database: testdb
// no SSL
// create table
// def url = 'jdbc:hsqldb:mem:yourDB'
//
def url = 'jdbc:mysql://localhost:3306/testdb?autoReconnect=true&useSSL=false'
def user = 'user1'
def password = 'password'
// def driver = 'org.hsqldb.jdbcDriver'
def driver = 'com.mysql.jdbc.Driver'
def sql = Sql.newInstance(url, user, password, driver)

// drop table 
sql.execute('DROP TABLE mytable2')

// create table
// sql.execute('create table mytable2(id INT NOT NULL AUTO_INCREMENT, name VARCHAR(15) NOT NULL, email VARCHAR(15), PRIMARY KEY(id))')

// use 'sql' instance ...

sql.close()

print("end it")
print('cool')
