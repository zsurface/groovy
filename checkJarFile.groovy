import static classfile.Print.*;
import static classfile.Aron.*;
import classfile.StopWatch;


var path = concatValidPath(list(getEnv("b"), "java"))
var lsPath = map( x -> x, lsCurr(path))
pp(lsPath)
for(String s : lsPath){
    if(containsSubstr(s, "AronLib.jar")){
	fl()
	pb("s=" + s)
	pb("path=" + path)
	run("notify.sh 'AronLib.jar' 'remove $b/java/AronLib.jar'")
    }
}


