import static classfile.Print.*;
import static classfile.Aron.*;
import classfile.StopWatch;

import groovy.sql.Sql
import java.sql.*
import java.io.File

import com.mysql.jdbc.*

StopWatch sw = new StopWatch();
/**
     KEY: Use LOAD DATA INFILE, mysql LOAD DATA INFILE, mysql batch insert, batch insert 
 
     USER: user1
     PASSWD: password
     DATABASE: testdb
     NO SSL
     TABLE: testtable1
 
     /tmp/data.csv
 
     FieldName
     "/usr/local"
     "/tmp"
*/
def url = 'jdbc:mysql://localhost:3306/testdb?autoReconnect=true&useSSL=false'
def user = 'user1'
def password = 'password'
// def driver = 'org.hsqldb.jdbcDriver'
def driver = 'com.mysql.jdbc.Driver'
def sql = Sql.newInstance(url, user, password, driver)


String pathToFileName(String path, int k){
    String s = fileName(path) + "_" + k + ".txt"
    return s
}
/**
     abaa
   
     a b
     a x
     a b a
     a x |
     a b a a
     a b x |
     a b x  => a b a

     a a b b
     x |
     x   |
     a   x |
     a   x   E
     a   b


     a a a b b
           |
     a a b b
     c   |
     
 */

var fPath = getEnv("b") + "/publicfile" + "/pathToDatabase.txt"
var pathdir = getEnv("b") + "/publicfile" + "/pathdir"
List<String> lsPath = readFile(fPath)

int k = 0
for(String path : lsPath){
    List<String> fList = list()
    String dirPath = pathdir + "/" + fileName(path)
    pb("dirPath=" + dirPath)
    listAllFile(path, fList)
    var p = dirPath + "_" + k + ".txt"
    pb("p=" + p)
    writeFileList(p, map( x -> x + "\n", fList))
    k++
}

sql.execute('DROP TABLE IF EXISTS testtable1')

sql.execute('CREATE TABLE testtable1(id INT NOT NULL AUTO_INCREMENT, path VARCHAR(1000) NOT NULL, PRIMARY KEY(id))')

var flist = lsFile(pathdir)
for(String fp : flist){
    def cmd = "LOAD DATA INFILE '" + fp + "' INTO TABLE testtable1(path)"
// sql.execute("LOAD DATA INFILE '/tmp/usr.txt' INTO TABLE testtable1(path)")
    sql.execute(cmd)
}

// use 'sql' instance ...

sql.close()

sw.printSec();

