import static classfile.Print.*;
import static classfile.Aron.*;
import classfile.StopWatch;

class MyClass{
    public MyClass(){
    }
}

def c = new MyClass();

def name = "dog"
println name

for(int i=0; i<10; i++){
    p(name);
}

def sw = new StopWatch();


def arr = ["dog", "cat", "fox"]

writeFileAppend("/tmp/f1.x", arr)

def str = executeCommand("ls /tmp");
p("str=" + str);

List<String> ls = listDir("/tmp");
p(ls);

def lss = of("dog", "cat");
p(lss);

def m = ["dog": 1, "cat": 3, "kk": 4]
p(m);

p arr


def map = new HashMap<String, Integer>();

map.put("dog", 1);
p(map);

def list = readFile "/tmp/f1.x"
for(String s : list){
    def ns = replaceStr(s, "dog", "cat")
    p(ns)
}

def ld = listFileDir("/Users/cat/myfile/bitbucket/java")
def mylist = []
for(String s : ld){
    if(isMatched('\\.java$', s)){
        mylist.add(s)
    }
}
fl()
p(mylist)


sw.printTime();

Scanner scan = new Scanner(System.in);
String s = scan.nextLine()

